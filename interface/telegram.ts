import * as config from '../config.json';
import {Manga, MangaList} from '../parser/simply';

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

import Telegraf from "telegraf";

export async function start() {
    const mangaList = new MangaList();
    const mangaL = await mangaList.getByTags('anal');
    const manga = await mangaList.getManga(mangaL[0].href);
    const bot = new Telegraf(config.telegram.token);
    bot.command('simply', async(ctx: any) => {
            function chunk(array: any[], size: number) {
                const numChunks = Math.ceil(array.length / size);
                const chunks = new Array(numChunks);

                for (let i = 0, o = 0; i < numChunks; ++i, o += size) {
                    chunks[i] = array.slice(o, size + o)
                }

                return chunks
            }
            const test1 = chunk(manga, 10);

            for (const i of test1){
                await sleep(500);
                const mangas = i.map((element: string) =>
                    ({
                        media: {url: element},
                        caption: "",
                        type: 'photo'
                    })
                );

                const test2 = await ctx.replyWithMediaGroup(mangas);
            }
            return;
        }
    );
    bot.launch();
}