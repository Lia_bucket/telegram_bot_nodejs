import * as cheerio from 'cheerio';
import axios from 'axios';

export class Manga {
    protected url: string;
    protected mangaURL: string;

    constructor() {
        this.url = '';
        this.mangaURL = 'https://api.simply-hentai.com/v1/images/album/'
    }

    async getManga(url: string): Promise<string[]> {
        this.mangaURL += url.split('/').slice(-1)[0];
        const tagsHtml = await axios.get(this.mangaURL);
        return Object.values(tagsHtml.data).map((element: any) => element.sizes.full)
    }
}

export class MangaList extends Manga{
    protected url: string;

    constructor() {
        super();
        this.url = 'https://www.simply-hentai.com/';
    }

    async get(params: GetParams): Promise<Array<MangaHref>> {
        const tagsHtml = await axios.get(this.url);
        const tagsUnparsed = await cheerio.load(tagsHtml.data);

        const hrefElements = tagsUnparsed('.col-xs-6 .object-title a');
        const manga: Array<MangaHref> = [];
        hrefElements.each((i, el) =>
            manga.push({ href: el.attribs.href as string, name: el.children[0].data as string })
        );

        return manga;
    }

    async getByTags(tags: string): Promise<Array<MangaHref>>{
        tags = tags[-1] === '/'? tags: tags + "/";
        this.url += tags ? `tag/${tags}` : tags;
        return this.get({tags})
    }
}

export class Tags extends MangaList {
    private readonly page: number;
    private tags: string;

    constructor() {
        super();
        this.page = 1;
        this.tags = 'tags/';
    }

    async get(params: GetParams): Promise<Array<MangaHref>> {
        if (params.tags) {

        } else {
            this.url += params.letter ? `letter/${params.letter}/`: '';
            this.url += params.sort !== undefined ? `sort/${Sorting[params.sort as number]}/`: '';
        }

        this.url += this.page ? `page/${this.page}`: '';
        return super.get(params)
    }
}

interface GetParams {
    page?: number,
    tags?: string,
    sort?: Sorting | null,
    letter?: string | null,
}

interface MangaHref {
    href: string,
    name: string
}

enum Sorting {
    popularity,
    views,
    alphabetical
}